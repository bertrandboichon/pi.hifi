
PiFi is a set of python scripts running as daemons to improve the experience of mpd on Raspberry Pi.
It has been tested with Volumio 1.4 and it adds:

* Support for IR remote control.
  You can use a small IR remote control (e.g. apple) along with a Flirc USB module acting as a keyboard with the raspberry pi.

* Support for LCD display (16x2 from Adafruit).
  It displays track, volume information as well as the current audio level (RMS) on screen.

* Switch from Airplay to Mpd and vice versa, display Airplay metadata on screen

* Improve overall responsiveness of display / control (new LCD controller class)


Find more on: https://bitbucket.org/bertrandboichon/

Repo owner or admin: Bertrand Boichon

Copyright (C) 2014 - Bertrand Boichon (b.boichon@gmail.com)